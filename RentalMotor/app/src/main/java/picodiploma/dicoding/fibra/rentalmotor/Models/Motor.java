package picodiploma.dicoding.fibra.rentalmotor.Models;

public class Motor {

    private String NamaMotor;
    private String Merek;
    private int Tahun;
    private String PlatNo;
    private int Harga;
    private String Deskripsi;
    private String Photo;

    public String getNamaMotor() {
        return NamaMotor;
    }

    public void setNamaMotor(String namaMotor) {
        NamaMotor = namaMotor;
    }

    public String getMerek() {
        return Merek;
    }

    public void setMerek(String merek) {
        Merek = merek;
    }

    public int getTahun() {
        return Tahun;
    }

    public void setTahun(int tahun) {
        Tahun = tahun;
    }

    public String getPlatNo() {
        return PlatNo;
    }

    public void setPlatNo(String platNo) {
        PlatNo = platNo;
    }

    public int getHarga() {
        return Harga;
    }

    public void setHarga(int harga) {
        Harga = harga;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        Photo = photo;
    }
}
