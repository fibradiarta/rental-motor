package picodiploma.dicoding.fibra.rentalmotor.Models;

import java.util.ArrayList;

public class MotorData {

    public static String[][] data = new String[][]{
            {"New Fino","2019","Yamaha","65000","B 1234 AZ","Yamaha New Fino 125 cc memiliki Mesin 125cc dengan teknologi Blue Core yang membuat motor Irit, Halus, dan Nyaman. Fitur canggih dengan fungsi ganda untuk menemukan lokasi dan membuka penutup kunci. Aman ketika harus mengunci rem saat berhenti di tanjakan atau turunan. Praktis, cukup dengan satu jari.","https://www.yamaha-motor.co.id/uploads/products/T7AUmYR6d7VYc9SWLZIp.png"},
            {"PCX 150 eSP","2019","Honda","150000","B 3451 AF","Honda PCX 150 eSP merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda PCX ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/36418.png"},
            {"New Scoopy","2019","Honda","70000","B 4576 ER","Honda Scoopy merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda Scoopy ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/fitures/85375.png"},
            {"Vario 125 eSP","2019","Honda","85000","B 6575 GH","Honda Vario 125 eSP merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda Vario ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/fitures/65188.png"},
            {"Vario eSP","2018","Honda","65000","B 7869 DF","Honda Vario eSP merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda Vario ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/fitures/59426.png"},
            {"Beat eSP","2018","Honda","60000","B 8604 GH","Honda Beat eSP merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda Beat ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/fitures/59638.png"},
            {"Beat Street eSP","2019","Honda","70000","B 2576 KL","Honda Beat Street eSP merupakan motor kekinian yang akan membuat penampilan anda menjadi semakin kece. Dengan teknologi mutakhir honda Beat Street ini dapat memberikan pengalaman yang sangat menyenangkan bagi anda.","https://cdnhonda.azureedge.net/uploads/products/fitures/95311.png"},
            {"Mio M3 125 AKS","2018","Yamaha","65000","B 7082 AD","Yamaha Mio M3 125 AKS memiliki Mesin 125cc dengan teknologi Blue Core yang membuat motor Irit, Halus, dan Nyaman. Fitur canggih dengan fungsi ganda untuk menemukan lokasi dan membuka penutup kunci. Aman ketika harus mengunci rem saat berhenti di tanjakan atau turunan. Praktis, cukup dengan satu jari.","https://www.yamaha-motor.co.id/uploads/products/8JZ7X67pntcf5Tk1z7DK.png"},
            {"Lexy VVA","2018","Yamaha","150000","B 5691 FG","Yamaha Lexy VVA memiliki teknologi Blue Core yang membuat motor Irit, Halus, dan Nyaman. Fitur canggih dengan fungsi ganda untuk menemukan lokasi dan membuka penutup kunci. Aman ketika harus mengunci rem saat berhenti di tanjakan atau turunan. Praktis, cukup dengan satu jari.","https://www.yamaha-motor.co.id/assets/lexi/img/features-01.png"},
            {"NMAX 155","2018","Yamaha","100000","B 6090 PF","Yamaha NMAX memiliki teknologi Blue Core yang membuat motor Irit, Halus, dan Nyaman. Fitur canggih dengan fungsi ganda untuk menemukan lokasi dan membuka penutup kunci. Aman ketika harus mengunci rem saat berhenti di tanjakan atau turunan. Praktis, cukup dengan satu jari.","https://www.yamaha-motor.co.id/uploads/products/fhyVu02ES4HYRmAHGa8D.png"},
            {"Vespa Sprint S","2019","Vespa","170000","B 7649 AA","Vespa Sprint S merupakan produk otomotif keluaran terbaru dari Vespa Piaggio yang akan membuat penampilanmu semakin elegant dengan tampilan fisik yang sangat Elegant dan nyaman.","https://www.vespa.com/dam/jcr:aba18860-ee5a-4a92-b124-cb3ca55da8d2/Sprint-S-black-1.png"},
            {"Vespa Primavera", "2018", "Vespa","150000","B 7832 DD","Vespa Primavera merupakan produk otomotif keluaran terbaru dari Vespa Piaggio yang akan membuat penampilanmu semakin elegant dengan tampilan fisik yang sangat Elegant dan nyaman.","https://www.vespa.com/dam/jcr:562512ba-a52d-4456-84fa-08c4c7380601/Primavera-biege-1.png"}

    };

    public static ArrayList<Motor> getListData(){
        ArrayList<Motor> list = new ArrayList<>();
        for(String[] DataMotor : data)
        {
            Motor motor = new Motor();
            motor.setNamaMotor(DataMotor[0]);
            motor.setTahun(Integer.parseInt(DataMotor[1]));
            motor.setMerek(DataMotor[2]);
            motor.setHarga(Integer.parseInt(DataMotor[3]));
            motor.setPlatNo(DataMotor[4]);
            motor.setDeskripsi(DataMotor[5]);
            motor.setPhoto(DataMotor[6]);

            list.add(motor);
        }

        return list;
    }
}
