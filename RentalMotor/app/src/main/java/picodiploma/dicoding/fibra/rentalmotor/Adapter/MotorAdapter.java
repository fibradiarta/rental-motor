package picodiploma.dicoding.fibra.rentalmotor.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import picodiploma.dicoding.fibra.rentalmotor.DetailsActivity;
import picodiploma.dicoding.fibra.rentalmotor.Models.Motor;
import picodiploma.dicoding.fibra.rentalmotor.R;

public class MotorAdapter  extends RecyclerView.Adapter<MotorAdapter.MotorViewHolder> {

    private ArrayList<Motor> listMotor;
    java.util.List<Motor> data= Collections.emptyList();
    private Context context;
    public String details_judul, details_merk, details_deskripsi, details_platno, details_photo;
    public int details_harga, details_tahun;

    //declare interface
    private OnItemClicked onClick;

    public OnItemClicked getOnClick() {
        return onClick;
    }

    public void setOnClick(OnItemClicked onClick) {
        this.onClick = onClick;
    }

    public MotorAdapter(ArrayList<Motor> listMotor) {
        this.listMotor = listMotor;
    }

    @NonNull
    @Override
    public MotorViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_motor, viewGroup, false);
        return new MotorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MotorViewHolder holder, final int position) {
        final Motor motor = listMotor.get(position);

        /*format indonesia*/
        Locale localeID = new Locale("in","ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        holder.judul.setText(motor.getNamaMotor());
        holder.tahun.setText(String.valueOf(motor.getTahun()));
        holder.merk.setText(motor.getMerek());
        holder.harga.setText(String.valueOf(formatRupiah.format(motor.getHarga())));

        Glide.with(holder.itemView.getContext())
                .load(motor.getPhoto())
                .apply(RequestOptions.placeholderOf(R.color.colorPrimary))
                .into(holder.photo);

        holder.btnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*untuk details*/
               /* details_judul = motor.getNamaMotor();
                details_merk = motor.getMerek();
                details_tahun = motor.getTahun();
                details_deskripsi = motor.getDeskripsi();
                details_photo = motor.getPhoto();
                details_platno = motor.getPlatNo();
                details_harga = motor.getHarga();

                passData(details_judul,details_merk,details_photo,details_deskripsi,details_tahun,details_harga,details_photo);*/

                getOnClick().onItemClick(listMotor.get(holder.getAdapterPosition()));

            }
        });
    }

    @Override
    public int getItemCount() {
        return listMotor.size();
    }

    public class MotorViewHolder extends RecyclerView.ViewHolder {

        TextView judul, merk, tahun, deskripsi, harga;
        ImageView photo;
        Button btnDetails;

        public MotorViewHolder(@NonNull View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.item_title);
            merk = itemView.findViewById(R.id.item_merk);
            tahun = itemView.findViewById(R.id.item_year);
            /*deskripsi = itemView.findViewById(R.id);*/
            harga = itemView.findViewById(R.id.item_price);
            photo = itemView.findViewById(R.id.item_poster);
            btnDetails = itemView.findViewById(R.id.btn_details);
        }
    }

    private void passData(String pass_judul, String pass_merk, String pass_deskripsi, String pass_photo, int pass_tahun, int pass_harga, String pass_platno)
    {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra("judul",pass_judul);
        intent.putExtra("merk", pass_merk);
        intent.putExtra("deskripsi",pass_deskripsi);
        intent.putExtra("photo",pass_photo);
        intent.putExtra("tahun",pass_tahun);
        intent.putExtra("harga",pass_harga);
        intent.putExtra("platno",pass_platno);

        context.startActivity(intent);
    }

    public interface OnItemClicked {
        void onItemClick(Motor motor);
    }

    public void searchingList(List<Motor> search)
    {
        listMotor = new ArrayList<>();
        listMotor.addAll(search);
        notifyDataSetChanged();
    }

}
