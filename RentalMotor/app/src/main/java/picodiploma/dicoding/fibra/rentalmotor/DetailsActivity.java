package picodiploma.dicoding.fibra.rentalmotor;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class DetailsActivity extends AppCompatActivity {

    private TextView tvJudul, tvDeskripsi, tvHarga, tvPlatno, tvTahun, tvMerek;
    private EditText edTglMulai, edTglSelesai;
    private ImageView imgPhoto;
    private Button btnSewa;
    private String photo, namaMotor;
    final Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Intent intent = getIntent();
        final Bundle bundle = intent.getExtras();

        /*format indonesia*/
        Locale localeID = new Locale("in","ID");
        final NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        //region inisialisasi view
        /*get data from main activity*/
        tvMerek = findViewById(R.id.get_merek);
        tvHarga = findViewById(R.id.get_harga);
        tvPlatno = findViewById(R.id.get_platno);
        tvTahun = findViewById(R.id.get_tahun);
        tvDeskripsi = findViewById(R.id.get_diskripsi);
        imgPhoto = findViewById(R.id.img_photo);
        edTglMulai = findViewById(R.id.tgl_mulai);
        edTglSelesai = findViewById(R.id.tgl_selesai);
        btnSewa = findViewById(R.id.btn_sewa);
        //endregion

        //region Collapse Layout
        /*collapsing*/
        namaMotor =(String) bundle.get("judul");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(namaMotor);

        collapsingToolbarLayout.setCollapsedTitleTextColor(
                ContextCompat.getColor(this, R.color.colorWhite));
        collapsingToolbarLayout.setExpandedTitleColor(
                ContextCompat.getColor(this, R.color.colorWhite));
        //endregion

        //region Tanggal
        /*tanggal*/
        final DatePickerDialog.OnDateSetListener dateMulai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateMulai();
            }
        };

        final DatePickerDialog.OnDateSetListener dateSelesai = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDateSelesai();
            }
        };

        edTglMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DetailsActivity.this,dateMulai,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        edTglSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(DetailsActivity.this,dateSelesai,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //endregion

        //region Bundle
        /*kondisi untuk bundle*/
        if(bundle != null)
        {
            photo =  bundle.getString("photo");
            Glide.with(DetailsActivity.this)
                    .load(photo)
                    .into(imgPhoto);
            tvPlatno.setText(bundle.getString("platno"));
            tvMerek.setText(bundle.getString("merek"));
            tvTahun.setText(String.valueOf(bundle.getInt("tahun")));
            tvHarga.setText(String.valueOf(formatRupiah.format(bundle.getInt("harga")))+"/hari");
            tvDeskripsi.setText(bundle.getString("deskripsi"));

        }
        //endregion

        //region Total Sewa
        btnSewa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mulai = edTglMulai.getText().toString();
                String selesai = edTglSelesai.getText().toString();

                long totalHari = 0;
                int totalHarga = 0;

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date tgl_mulai = new Date();
                Date tgl_selesai = new Date();

                try{
                    tgl_mulai = format.parse(mulai);
                    tgl_selesai = format.parse(selesai);

                    if(mulai.isEmpty() && selesai.isEmpty())
                    {
                        Toast.makeText(DetailsActivity.this,"Silahkan input tanggal mulai dan selesai sewa",Toast.LENGTH_LONG).show();
                    }else
                    {
                        if(tgl_mulai.getTime() > tgl_selesai.getTime())
                        {
                            Toast.makeText(DetailsActivity.this,"Format tanggal tidak sesuai, pastikan tanggal mulai tidak lebih dari tanggal selesai",Toast.LENGTH_LONG).show();
                        }else
                        {
                            long diff = tgl_selesai.getTime() - tgl_mulai.getTime();
                            totalHari = TimeUnit.DAYS.convert(diff,TimeUnit.MILLISECONDS);
                            totalHarga = Integer.parseInt(String.valueOf(totalHari))*Integer.parseInt(String.valueOf(bundle.getInt("harga")));
                            Toast.makeText(DetailsActivity.this,"Anda telah menyewa motor "+namaMotor+" selama "+totalHari+" hari dengan total harga "+formatRupiah.format(totalHarga),Toast.LENGTH_LONG).show();
                        }
                    }

                }catch (ParseException ex)
                {
                    ex.printStackTrace();
                }

            }
        });
        //endregion
    }

    private void updateDateMulai()
    {
        String myformat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myformat, Locale.US);

        edTglMulai.setText(sdf.format(calendar.getTime()));
    }

    private void updateDateSelesai()
    {
        String myformat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myformat, Locale.US);

        edTglSelesai.setText(sdf.format(calendar.getTime()));
    }

}
