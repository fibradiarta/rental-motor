package picodiploma.dicoding.fibra.rentalmotor;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import picodiploma.dicoding.fibra.rentalmotor.Adapter.MotorAdapter;
import picodiploma.dicoding.fibra.rentalmotor.Models.Motor;
import picodiploma.dicoding.fibra.rentalmotor.Models.MotorData;

public class MainActivity extends AppCompatActivity implements MotorAdapter.OnItemClicked, SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private Toolbar toolbar;
    private ArrayList<Motor> list = new ArrayList<>();
    public MotorAdapter motorAdapter;
    private String searchNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("FD Rent");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);

        recyclerView = findViewById(R.id.rv_listmotor);
        recyclerView.setHasFixedSize(true);

        list.addAll(MotorData.getListData());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        motorAdapter = new MotorAdapter(list);
        recyclerView.setAdapter(motorAdapter);

        motorAdapter.setOnClick(MainActivity.this);
       /* showListMotor();*/
    }

    private void showListMotor()
    {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        MotorAdapter motorAdapter = new MotorAdapter(list);
        recyclerView.setAdapter(motorAdapter);

        motorAdapter.setOnClick(MainActivity.this);
    }

    @Override
    public void onItemClick(Motor motor) {
        Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
        intent.putExtra("judul", motor.getNamaMotor());
        intent.putExtra("photo", motor.getPhoto());
        intent.putExtra("deskripsi", motor.getDeskripsi());
        intent.putExtra("merek",motor.getMerek());
        intent.putExtra("tahun",motor.getTahun());
        intent.putExtra("platno",motor.getPlatNo());
        intent.putExtra("harga",motor.getHarga());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.toolbar_menu,menu);

        MenuItem menuItem = menu.findItem(R.id.action_search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String Input = s.toLowerCase();
        ArrayList<Motor> newList = new ArrayList<>();

        for(Motor data : list)
        {
            String NamaMotor = data.getNamaMotor().toLowerCase();
            String Merek = data.getMerek().toLowerCase();
            String Tahun = String.valueOf(data.getTahun()).toLowerCase();
            String Harga = String.valueOf(data.getHarga());

            if(NamaMotor.contains(Input) || Merek.contains(Input) || Tahun.contains(Input) || Harga.contains(Input))
            {
                newList.add(data);
            }
        }
        motorAdapter.searchingList(newList);
        return true;
    }
}
